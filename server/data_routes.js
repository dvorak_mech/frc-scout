// routes for event overview dashboard

async function routes (fastify, options) {

  fastify.get('/api/events/:event_id/matches/schedule', function (request, reply) {
    
  })

  fastify.get('/api/events/:event_id/matches/data', function (request, reply) {
    
  })

  fastify.get('/api/events/:event_id/matches/data/:start/:end', function (request, reply) {
    
  })

  // ----- Admin Routes ----- //
  fastify.post('/api/events/:event_id/create', function (request, reply) {
    
  })

  fastify.post('/api/events/:event_id/delete', function (request, reply) {
    
  })

}

module.exports = routes
/*
 * /api/events/:event_id/matches/schedule (score results as available, otherwise just blank scores)
 * /api/events/:event_id/matches/data (full match scouting data)
 * /api/events/:event_id/matches/data/:start/:end
 *         --- ADMIN ---
 * /api/events/:event_id/create
 * /api/events/:event_id/delete
 */
