async function routes (fastify, options) {

  fastify.get('/api/dispatch/:event_id/request', function (request, reply) {
    
  })

  fastify.post('/api/dispatch/:event_id/report', function (request, reply) {
    
  })

  fastify.post('/api/login', function (request, reply) {

  })

  fastify.post('/api/register', function (request, reply) {

  })

}

module.exports = routes
/* ------------ API -------------
 *         --- USERS ---
 * /api/dispatch/:event_id/request
 * /api/dispatch/:event_id/report
 */
